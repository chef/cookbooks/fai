# coding: iso-8859-15
# support for installing GSI nfsroots

# install davfs package into nfsroot
node.default['fai']['common_extra_pkgs'] << "davfs2"


# now include the default recipe:
include_recipe 'fai'
