#
# Cookbook:: fai
# Recipe:: default
#
# Copyright:: 2011-2024 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Bastian Neuburger   <b.neuburger@gsi.de>
#  Christopher Huhn    <c.huhn@gsi.de>
#  Dennis Klein        <d.klein@gsi.de>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# We need an nfs server, make sure to set the exports on the node/in the role
include_recipe "nfs::server"

ohai 'Packages' do
  plugin 'packages'
  action :nothing
end

# install required packages
package "fai-server" do
  notifies :reload, 'ohai[Packages]', :immediate
end
#package fai-quickstart ???

# fai-setup calls fai-mk-configspace and that will demand
#  fai-doc unless a configspace already exists
package "fai-doc"

# fallback to admit access to everybody if not specified
node.default['fai']['clients'] = [ "*" ] if node['fai']['clients'].empty?

if node['fai']['configdir']
  directory node['fai']['configdir'] do
    recursive true
    owner 'root'
    group 'root'
    mode '0755'
    action :create
    # TODO: fill from version control or sample scripts
  end

  node['fai']['clients'].each do |client|
    nfs_export '/srv/fai/config' do
      network client
      writeable false
      sync false
      options ['subtree_check','no_root_squash']
    end
  end

  node['fai']['insecure_clients'].each do |insecure_client|
    nfs_export '/srv/fai/config' do
      network insecure_client
      writeable false
      sync false
      options ['subtree_check','no_root_squash','insecure']
    end
  end
end

# setup default fai config:
fai_config 'default' do
  nfsroot_config_dir '/etc/fai'
  nfsroot_dir        '/srv/fai/nfsroot'
  tftpdir ['/srv/tftp', node['lsb']['codename'],
           # FIXME: collected by the Debian Ohai plugin from sys
           node['debian']['architecture'],
           # install kernel into a flavor specific subdir for distinction:
           "fai#{node['packages']['fai-server']['version']}"
          ].join('/')
  hook_dir node['fai']['nfsroot-hooks']
  additional_sources node['fai']['additional_sources']

  notifies :run, 'execute[update-nfsroot]', :immediately
end

# update the nfsroot:
execute "update-nfsroot" do
  command "fai-make-nfsroot -k"
  action :nothing
  only_if { Dir.exist?('/srv/fai/nfsroot') && node['fai']['handle_nfsroots'] }
end

# TODO: add cron job for periodic updating of nfsroot

# optionally setup logging ($LOGUSER)

# build nfsroot(s),
# copy demo config from package fai-doc,
# send kernels and initrds to the TFTP server
if node['fai']['setup_default']

  # TODO: in the meantime there's a fai-mk-configspace command
  execute "copy demo config" do
    creates "/srv/fai/config/scripts"
    command "cp -R /usr/share/doc/fai-doc/examples/simple/* /srv/fai/config"
    action :run
  end

  execute "fai-setup" do
    creates "/srv/fai/nfsroot"
    notifies :restart, "service[#{node['nfs']['service']['server']}]"
    action :run
    only_if { node['fai']['handle_nfsroots'] }
  end
end

node['fai']['clients'].each do |client|
  nfs_export '/srv/fai/nfsroot' do
    network client
    writeable false
    sync false
    options ['subtree_check', 'no_root_squash']
    only_if 'test -d /srv/fai/nfsroot'
  end
end

node['fai']['insecure_clients'].each do |insecure_client|
  nfs_export '/srv/fai/nfsroot' do
    network insecure_client
    writeable false
    sync false
    options ['subtree_check', 'no_root_squash','insecure']
    only_if 'test -d /srv/fai/nfsroot'
  end
end
