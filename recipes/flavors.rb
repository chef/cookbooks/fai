#
# Cookbook:: fai
# Recipe:: flavors
#
# Copyright:: 2012-2024 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Christopher Huhn    <c.huhn@gsi.de>
#  Dennis Klein        <d.klein@gsi.de>
#  Bastian Neuburger   <b.neuburger@gsi.de>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# recipe for configuring multiple FAI nfsroots on a single server in parallel
#

directory '/etc/fai/flavors' do
  action :create
end

nfsroot_config = 'nfsroot.conf'
nfsroot_cmd = 'fai-make-nfsroot'

# If fai.config_source is not set, fall back to a default nfs URL
unless node['fai']['config_source'] == nil
  fai_config_source = "nfs://#{node['ipaddress']}:#{node['fai']['configdir']}"
else
  fai_config_source = node['fai']['config_source']
end


node['fai']['flavors'].each do |f, config|

  config = config.to_h

  # apply some defaults:

  configdir = "/etc/fai/flavors/#{f}"

  # f is something like squeeze-amd64
  codename = config['codename'] || f.split("-")[0]
  arch     = config['arch']     || f.split("-")[1]
  # FIXME: handle flavors like "codename-arch-bla-blubb"
  suffix = (f.split("-")[2..-1] || []).join('_')

  nfs_dir = "/srv/fai/nfsroot-#{f}"

  if config['tftpdir']
    tftpdir = config['tftpdir']
  else
    tftpdir = "/srv/tftp/#{codename}/#{arch}" +
              ( (suffix.empty?) ? '' : "/#{suffix}" ) +
              "/fai#{node['packages']['fai-server']['version']}/"
  end

  # only create config in /etc/fai/flavors or also
  #  create the installer nfsroot with fai-make-nfsroot?
  config['config_only'] ||= (node['fai']['handle_nfsroots'] == false)

  #
  # create the config in /etc/fai/flavors:
  #

  directory configdir do
    action :create
  end

  directory "#{configdir}/apt" do
    action :create
  end

  template "#{configdir}/fai.conf" do
    source "fai.conf.erb"
    variables({
      :config_src => fai_config_source
    })
    mode 0644
  end

  template "#{configdir}/#{nfsroot_config}" do
    source "make-fai-nfsroot.conf.erb"
    variables({
      :config_src => fai_config_source,
      :codename   => codename,
      :debmirror_url => config['debmirror_url'] || node['fai']['debmirror_url'],
      :nfsdir     => nfs_dir,
      # install kernel into a flavor specific subdir for distinction:
      :tftpdir    => tftpdir,
      :arch       => arch,
      :hookdir    => config['hookdir'] || "/etc/fai/flavors/#{f}/nfsroot-hooks/"
    })
    mode 0644
    notifies :run, "execute[update-#{f}-nfsroot]"
  end

  template "#{configdir}/apt/sources.list" do
    source "fai_sources.list.erb"
    variables({
      :codename => codename,
      components: case codename
                  when 'bullseye','buster','stretch','jessie' #,...
                    'main contrib non-free'
                  else
                    'main contrib non-free non-free-firmware'
                  end,
      :debmirror_url => config['debmirror_url'] || node['fai']['debmirror_url'],
      :additional_sources => config['additional_sources'] || []
    })
    mode 0644
  end

  template "#{configdir}/apt/apt.conf" do
    source "fai_apt.conf.erb"
    variables(
      # we cannot simply use 'or' here, as we are dealing with booleans ...
      install_recommends: config.has_key?('install_recommends') ?
        config['install_recommends'] :
        node['fai']['install_recommends'],
      install_suggests: config.has_key?('install_suggests') ?
        config['install_suggests'] :
        node['fai']['install_suggests']
    )
  end

  link "#{configdir}/live.conf" do
    to '/etc/fai/live.conf'
  end

  pkgs = config['pkgs'] || [ ]
  pkgs += node['fai']['common_extra_pkgs']

  template "#{configdir}/NFSROOT" do
    source "NFSROOT.erb"
    variables(
      :extra_pkgs => pkgs,
      :install_action => ((codename == 'stretch') ? 'install' : nil)
    )
    mode 0644
    notifies :run, "execute[update-#{f}-nfsroot]"
  end

  #
  # update nfsroot
  #
  execute "update-#{f}-nfsroot" do
    command "#{nfsroot_cmd} -k -C #{configdir}"
    action :nothing
    # don't abort if nfsroot upgrades fail:
    ignore_failure true
    only_if { File.exist?(nfs_dir) }
    # resource has to be defined but will be skipped:
    not_if { config['config_only'] }
  end

  #
  # or create nfsroot:
  #
  # -l option is required for fai-cd with FAI <= 4.*
  execute "fai-setup -C #{configdir} #{'-l' if config['liveboot']}" do
    action :run
    creates nfs_dir
    # resource has to be defined but will be skipped:
    not_if { config['config_only'] }
  end

  # skip TFTP setup
  next if config['config_only']

  #
  # set up tftp dir
  #
  directory tftpdir do
    recursive true
  end

  #
  # add NFS entry:
  #
  node['fai']['clients'].each do |client|
    nfs_export nfs_dir do
      network client
      writeable false
      sync false
      options ['subtree_check', 'no_root_squash']
    end
  end

end
