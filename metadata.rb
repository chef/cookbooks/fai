name             "fai"
maintainer       'GSI IT department'
maintainer_email 'linuxgroup@gsi.de'
license          'Apache-2.0'
description      "Installs/Configures fai"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "2.0.0"
depends          "nfs"

source_url       'https://git.gsi.de/chef/cookbooks/fai'
issues_url       'https://git.gsi.de/chef/cookbooks/fai/issues'
