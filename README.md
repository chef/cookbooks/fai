Description
===========

Deployment for a FAI server. FAI provides fully automated Debian installations.

Recipes
=======

* `default` - Sets up a FAI-Server

Attributes
==========

* `config_src` -- Location of the fai config space, URLs of the following types are supported: nfs, file, cvs, cvs+ssh, svn+file, svn+http, git, git+http, hg+http, e.g. nfs://yourservername/path/to/config/space
* `debmirror_url` -- Mirror to use when building the fai nfs root
* `root_pw_hash` -- a password hash for root login on nodes during install time, generated e.g. using `makepasswd --crypt-md5`
* `root_ssh_pubkey` -- a ssh authorized_keys file for root login on nodes during install time
* `setup_default` -- prepares server for FAIing, if you do not change the `config_src` attribute


Further information
===================

* http://www.fai-project.org

Copyright
=========

2011 - 2024 – [GSI Helmholtzzentrum fuer Schwerionenforschung GmbH](https://gsi.de)

Authors
=======

- Bastian Neuburger   <b.neuburger@gsi.de>
- Christopher Huhn    <c.huhn@gsi.de>
- Dennis Klein        <d.klein@gsi.de>
