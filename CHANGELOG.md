# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/).

## [2.0.0] - 2024-10-24

### Addded
- test suite modernized
- support for `non-free-firmware` component on Debian Bookworm

### Changed
- Removed versioned dependency on `nfs` cookbook

### Fixed
- many small changes
- `node['debian']['packages']` → `node['packages']`
