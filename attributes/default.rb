# lines that will be added to the fai nfsroot's sources.list
default['fai']['additional_sources'] = [ ]

# clients that can connect: empty allows all clients
default_unless['fai']['clients']     = [ ]

# developer machines are allowed to mount from unprivileged ports (nated VMs)
default_unless['fai']['insecure_clients'] = [ ]

# packages to install into the nfsroots
default['fai']['common_extra_pkgs'] = [
  'cryptsetup',
  'lsb-release',
  'moreutils'
]

# dir for FAI config
default['fai']['configdir']          = "/srv/fai/config"

# the installer pulls its config from this location:
default_unless['fai']['config_source'] = "nfs://#{node['ipaddress']}:#{node['fai']['configdir']}"

# dir for FAI config
default['fai']['nfsroot-hooks']      = "/etc/fai/nfsroot-hooks"

# Debian repository for nfsroot creation
default_unless['fai']['debmirror_url'] = "http://deb.debian.org/debian"

# recommended packages should be installed:
default_unless['fai']['install_recommends'] = true
# while suggested packages should not:
default_unless['fai']['install_suggests']   = false

# root password
default['fai']['root_pw_hash']       = '$1$kBnWcO.E$djxB128U7dMkrltJHPf6d1'   # "fai"

# install the sample config:
default['fai']['setup_default']      = false

# set this to false to keep the FAI cookbook from
#  creating and updating nfsroots:
default_unless['fai']['handle_nfsroots'] = true

# this value will be set by the packages ohai plugin at install time
default['packages']['fai-server']['version'] = ''

# FIXME: this value will be set by the Debian ohai plugin from sys cookbook
#  as a default we will define it as empty
default['debian']['architecture'] = ''
