require 'spec_helper'

#  should be created
describe file('/etc/fai/flavors/default/') do
  it { should exist }
  it { should be_directory }
end

#  should be created
describe file('/srv/fai/nfsroot-default/') do
  it { should exist }
  it { should be_directory }
end

#  should be created
describe file('/etc/fai/flavors/stretch-amd64-createmenot/') do
  it { should exist }
  it { should be_directory }
end

#  should not be created
describe file('/srv/fai/nfsroot-stretch-amd64-createmenot/') do
  it { should_not exist }
end
