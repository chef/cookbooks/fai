#
# Cookbook:: fai
# Resource:: fai_config
#
# Copyright:: 2012-2024 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
#
# Authors:
#  Christopher Huhn    <c.huhn@gsi.de>
#  Dennis Klein        <d.klein@gsi.de>
#  Bastian Neuburger   <b.neuburger@gsi.de>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# additional source repos for package installation:
property :additional_sources, Array, default: []

# architecture eg. 'amd64'
property :arch, String

# codename eg. woody
property :codename, String, default: node['lsb']['codename']

# the URL the installer pulls its config from:
property :config_source, String,
         default: node['fai']['config_source']

# the location of the Debian repository
property :debmirror_url, String, default: node['fai']['debmirror_url']

# dir for installing nfsroot_hooks that are executed during nfsroot creation
property :hook_dir, String

# install recommended packages?
property :install_recommends, [TrueClass, FalseClass],
         default: node['fai']['install_recommends']

# install suggested packages?
property :install_suggests,   [TrueClass, FalseClass],
         default: node['fai']['install_suggests']

# the directory that holds the FAI config
property :nfsroot_config_dir, [String, NilClass]

# the base directory of the installer chroot
property :nfsroot_dir, [String, NilClass]

# list of packages to be installed into the installer nfsroot
property :pkgs, Array, default: []

# the directory kernel and initrs are copied to during installer creation
property :tftpdir, String

default_action :create

action :create do

  nfsroot_config_dir = new_resource.nfsroot_config_dir ||
                       "/etc/fai/flavors/#{new_resource.name}"

  nfsroot_dir = new_resource.nfsroot_dir ||
                "/srv/fai/nfsroot-#{new_resource.name}"

  # apply some defaults:
  nfsroot_config = 'nfsroot.conf'

  # new_resource.name is something like squeeze-amd64
  codename = new_resource.codename || new_resource.name.split("-")[0]
  arch     = new_resource.arch     || new_resource.name.split("-")[1]
  # FIXME: handle flavors like "codename-arch-bla-blubb"
  suffix = (new_resource.name.split("-")[2..-1] || []).join('_')

  if new_resource.tftpdir
    tftpdir = new_resource.tftpdir
  else
    begin
      fai_version = node['packages']['fai-server']['version']
    rescue NoMethodError
      fai_version = 'unknown'
    end

    tftpdir = "/srv/tftp/#{codename}/#{arch}" +
              ( "/#{suffix}" unless suffix.empty? ) +
              "/fai_#{fai_version}/"
  end

  #
  # create the config in /etc/fai/flavors:
  #
  directory nfsroot_config_dir do
    action :create
    recursive true
  end

  directory "#{nfsroot_config_dir}/apt" do
    action :create
  end

  template "#{nfsroot_config_dir}/fai.conf" do
    cookbook 'fai'
    source   'fai.conf.erb'
    variables(config_src: new_resource.config_source)
    mode 0o0644
  end

  template "#{nfsroot_config_dir}/#{nfsroot_config}" do
    cookbook 'fai'
    source   'make-fai-nfsroot.conf.erb'
    variables(
      config_src:    new_resource.config_source,
      codename:      codename,
      debmirror_url: new_resource.debmirror_url,
      nfsdir:        nfsroot_dir,
      # install kernel into a flavor specific subdir for distinction:
      tftpdir:       tftpdir,
      arch:          arch,
      hookdir:       new_resource.hook_dir ||
      "/etc/fai/flavors/#{new_resource.name}/nfsroot-hooks/"
    )
    mode 0o0644
  end

  template "#{nfsroot_config_dir}/apt/sources.list" do
    cookbook 'fai'
    source   'fai_sources.list.erb'
    variables(
      codename: codename,
      components: case codename
                  when 'bullseye','buster','stretch','jessie' #,...
                    'main contrib non-free'
                  else
                    'main contrib non-free non-free-firmware'
                  end,
      debmirror_url: new_resource.debmirror_url,
      additional_sources: new_resource.additional_sources
    )
    mode 0o0644
  end

  template "#{nfsroot_config_dir}/apt/apt.conf" do
    cookbook 'fai'
    source   'fai_apt.conf.erb'
    variables(
      install_recommends: new_resource.install_recommends,
      install_suggests:   new_resource.install_suggests
    )
  end

  link "#{nfsroot_config_dir}/live.conf" do
    to '/etc/fai/live.conf'
  end

  pkgs = new_resource.pkgs + node['fai']['common_extra_pkgs']

  template "#{nfsroot_config_dir}/NFSROOT" do
    cookbook 'fai'
    source   'NFSROOT.erb'
    variables(
      extra_pkgs: pkgs,
      install_action: nil
    )
    mode 0o0644
  end
end
